#include <stdio.h>
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <netdb.h>
    #include <string.h>
    #include <arpa/inet.h>
    #include <stdlib.h>
    #include <sys/stat.h>
    #include <unistd.h>
    #include <signal.h>
    #include <time.h>
    #include <syslog.h>

    #define PORT 80
    #define SIZE 8
    #define MSGSIZE 1024

      int readLine(int s, char*line, int *result_size){
        int acum=0, size;
        char buffer[SIZE];

        while ((size=read(s, buffer, SIZE)) > 0) {
            if (size < 0) return -1;
            strncpy(line+acum, buffer, size);
            acum += size;
            if(line[acum-1] == '\n' && line[acum-2] == '\r') {
                break;
            }
        }

        *result_size = acum;

        return 0;
    }

    int writeLine(int s, char *line, int total_size) {
        int acum = 0, size;
        char buffer[SIZE];

        if(total_size > SIZE) {
            strncpy(buffer, line, SIZE);
            size = SIZE;
        } else  {
            strncpy(buffer, line, total_size);
            size = total_size;
        }

        while( (size=write(s, buffer, size)) > 0) {
            if(size<0) return size;
            acum += size;
            if (acum >= total_size) break;

            size = ((total_size-acum)>=SIZE)?SIZE:(total_size-acum)%SIZE;
            strncpy(buffer, line+acum, size);
        }

        return 0;
    }

    int serve(int s) {
        char buff[2048];
        char *path = "html";
        char *archivo;
        char command[MSGSIZE];
        int size, r, nlc = 0, read_bytes;

        time_t t = time(NULL);
        struct tm tm = *localtime(&t);


        while(1) {


            r = readLine(s, command, &size);
            command[size-2] = 0;
              size-=2;
              strcat(buff,command);
              strcat(buff,"\n");
            if(command[size-1] == '\n' && command[size-2] == '\r') {
                break;
            }

        }
        printf("%s\n",buff);
        char *metodo;
        char *peticion;
        char aux[1024];

        metodo=strtok(buff," ");
        peticion=strtok(NULL," ");
        strcat(aux,path);
        strcat(aux,peticion);
        printf("%s\n",aux);
        FILE *fd = fopen(aux,"r");

        if(fd == NULL){
          int tamano;
          printf("No lo encontre");
          openlog("Error404", LOG_PID | LOG_CONS, LOG_USER);
          syslog(LOG_INFO, "El archivo %s no fue encontrado!\n", aux);
          closelog();
          FILE *error=fopen("html/error404.html", "r");


                 fseek(error, 0L, SEEK_END);
                 tamano = ftell(error);
                 fseek(error, 0L, SEEK_SET);


                 char *archivo = malloc(tamano+1);
                 fread(archivo, tamano, 1, error);
                 fclose(error);


                 sprintf(command, "HTTP/1.0 404 NOT FOUND\r\n");
                 writeLine(s, command, strlen(command));
                 sprintf(command, "Date: Fri, 31 Dec 1999 23:59:59 GMT\r\n");
                 writeLine(s, command, strlen(command));
                 sprintf(command, "Content-Type: text/html\r\n");
                 writeLine(s, command, strlen(command));
                 sprintf(command, "Content-Length: %d\r\n",tamano);
                 writeLine(s, command, strlen(command));
                 sprintf(command, "\r\n%s",archivo);
                 writeLine(s, command, strlen(command));

                 free(archivo);

        }else{
          printf("Si lo encontre\n");
          char *ext;
          char *djka;
          char content[128];

          strtok_r(peticion,".",&ext);
          printf("%s\n",aux);



          char *extensions[32] = {"gif","image/gif","txt","text/plain","jpg","image/jpg","jpeg","image/jpeg","png", "image/png","ico", "image/ico","zip", "image/zip","gz","image/gz","tar", "image/tar","htm", "text/html","html","text/html","css",
          "text/css","php", "text/html","pdf","application/pdf","zip","application/octet-stream","js","text/javascript"};

          int i = 0;

          for(i = 0; i < 32; i++) {
            if(strncmp(ext,extensions[i],strlen(extensions[i]))==0){
               strncpy(content,extensions[i+1],strlen(extensions[i+1]));
               break;
            }
          }

          struct stat buf;
          int sizeFile;
          stat(aux, &buf);
          sizeFile=buf.st_size;


          sleep(1);

          sprintf(command, "HTTP/1.0 200 OK\r\n");
          writeLine(s, command, strlen(command));

          sprintf(command, "Date: Fri, 13 May 2016 23:59:59 GMT\r\n");
          writeLine(s, command, strlen(command));

          sprintf(command, "Content-Type: %s\r\n",content);
          writeLine(s, command, strlen(command));

          sprintf(command, "Content-Length: %d\r\n",sizeFile);
          writeLine(s, command, strlen(command));

          sprintf(command, "\r\n");
          writeLine(s, command, strlen(command));
        char file[sizeFile];
        int suma = 0;
        size = fread(file, 1, sizeFile, fd);
        if(size<0){
          openlog("Fread Fallo linea 186 server.c", LOG_PID | LOG_CONS, LOG_USER);
          syslog(LOG_INFO, "Error en %s \n", file);
          closelog();
        }


          while( (size=write(s, &file[suma], size) > 0)) {
              suma += size;
              if (suma >= sizeFile) {
                break;
              }
              size = fread(file, 1, sizeFile, fd);
              if(size<0){
                openlog("Fread Fallo linea 199 server.c", LOG_PID | LOG_CONS, LOG_USER);
                syslog(LOG_INFO, "Error en %s \n", file);
                closelog();
              }
            }
        sync();
        }
    }

    int main() {

        int sd, sdo, addrlen, size, r;
        struct sockaddr_in sin, pin;

        // 1. Crear el socket
        sd = socket(AF_INET, SOCK_STREAM, 0);

        memset(&sin, 0, sizeof(sin));
        sin.sin_family = AF_INET;
        sin.sin_addr.s_addr = INADDR_ANY;
        sin.sin_port = htons(PORT);

        // 2. Asociar el socket a un IP/puerto
        r=bind(sd, (struct sockaddr *) &sin, sizeof(sin));
        	if(r<0){
    		perror("bind");
    		return -1;
    	}
    	// 3. Configurar el backlog
        listen(sd, 5);

        addrlen = sizeof(pin);
        // 4. aceptar conexiÃ³n
       signal(SIGCHLD,SIG_IGN);
        while( (sdo = accept(sd, (struct sockaddr *)  &pin, &addrlen)) > 0) {

             if(!fork()) {

                printf("Conectado desde %s\n", inet_ntoa(pin.sin_addr));
                printf("Puerto %d\n", ntohs(pin.sin_port));

                serve(sdo);

                close(sdo);
                exit(0);
             }
        }
        close(sd);

        sleep(1);

    }
